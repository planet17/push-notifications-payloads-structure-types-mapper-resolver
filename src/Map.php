<?php
/**
 * Project: Push Notifications: Payloads Structure Types
 * Author:  planet17
 */

namespace Planet17\PushNotifications\PayloadsStructureTypesMapperResolver;

use Planet17\PushNotifications\PayloadsStructureTypes\FCM\IOS;
use Planet17\PushNotifications\PayloadsStructureTypes\FCM\Simple as FCMSimple;
use Planet17\PushNotifications\PayloadsStructureTypes\FCM\WebCustom;
use Planet17\RulesMapResolver\Map as BaseMap;

/**
 * Class Map - Example of default class Map for use.
 *
 * @method Rule getPrototype()
 *
 * @package Planet17\PushNotifications\ChannelsRoutingResolver
 */
class Map extends BaseMap
{
    public function setUp()
    {
        $this->addRule(
            WebCustom::class,
            $this->getPrototype()
                 ->addPlatform('web')
                 ->addPlatform('webDesktop')
                 ->addPlatform('desktop')
                 ->addPlatform('mobile')
                 ->addPlatform('mobileSites')
                 ->addPlatform('webMobile')
        );

        $this->addRule(
            FCMSimple::class,
            $this->getPrototype()
                 ->addPlatform('android')
        );

        $this->addRule(
            IOS::class,
            $this->getPrototype()
                 ->addPlatform('ios')
                 ->addPlatform('apple')
        );
    }
}
