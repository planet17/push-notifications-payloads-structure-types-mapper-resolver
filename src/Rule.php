<?php
/**
 * Project: Push Notifications: Payloads Structure Types
 * Author:  planet17
 */

namespace Planet17\PushNotifications\PayloadsStructureTypesMapperResolver;


use Planet17\PushNotifications\ChannelsRoutingResolver\Rule as BaseRule;


/**
 * Class PlatformRule - Example of default class Rule for use.
 *
 * Class implements simple Rule what can be matching by two options:
 *
 * ```
 * 1) Platform of receiver
 * 2) Environment name where script will works.
 *
 * @package Planet17\PushNotifications\ChannelsRoutingResolver
 */
class Rule extends BaseRule
{
}
