<?php
/**
 * Project: Push Notifications: Payloads Structure Types
 * Author:  planet17
 */

namespace Planet17\PushNotifications\PayloadsStructureTypesMapperResolver;


use Planet17\RulesMapResolver\Resolvers\Simple;


/**
 * Class Resolver - Example of default class Resolver for use.
 *
 * @package Planet17\PushNotifications\ChannelsRoutingResolver
 */
class Resolver extends Simple
{
}
