<?php
/**
 * Project: Push Notifications: Payloads Structure Types
 * Author:  planet17
 */

namespace Planet17\PushNotifications\PayloadsStructureTypesMapperResolver;


use PHPUnit\Framework\TestCase;
use Planet17\PushNotifications\PayloadsStructureTypes\{FCM\IOS, FCM\Simple, FCM\WebCustom};


class ExampleTest extends TestCase
{
    /**
     * @dataProvider providerSetRuleAndReturn()
     *
     * @param string $classExpected
     * @param string $platform
     * @param string $environment
     */
    public function testReturnIOSType(string $classExpected, string $platform, string $environment)
    {
        $resolver = new Resolver(new Map(new Rule()));
        $current = [Rule::OPT_NAME_PLATFORM => $platform, Rule::OPT_NAME_ENVIRONMENT => $environment];
        $this->assertSame($classExpected, $resolver->resolve($current));
    }


    public function providerSetRuleAndReturn():array
    {
        return [
            'ios' => [IOS::class, 'ios', 'dev'],
            'simple' => [Simple::class, 'android', 'dev'],
            'custom' => [WebCustom::class, 'web', 'prod'],
        ];
    }
}
